# PADAWAN JAVA #01 #


# Conheça a nossa stack BackEnd #

* Java 8
* Spring Boot
* Spring Cloud
* JPA
* Postgres
* MongoDB
* ElasticSearch
* Apache Kafka
* AWS SQS
* AWS SNS


# O Teste #
## Criar uma API REST para um sistema de inventário ##

A sua API deverá ser capaz de realizar:

* As operações básicas de CRUD (Create, Read, Update and Delete) para um inventário;
	* GET /inventory - Lista todos os bens inventariados
	* POST /inventory - Cadastra um novo bem
	* PUT /inventory/:id - Altera os dados de um determinado bem
	* DELETE /inventory/:id - Exclui um bem
	* GET /inventory/:id - Lista os dados de um determinado bem
* Possuir uma chamada que calcule quantos itens já foram cadastrados até o momento.
	* GET /howmany - Retorna quantos bens já foram cadastrados
		* Nesse métodos, um bônus será a passagem de um parâmetro "complete" que poderá ser "true" ou "false" ou "all" e a API deveria filtrar por esse atributo ou trazer todos no caso de "all"
		* Ex: /howmany?complete=true
		* Ex: /howmany?complete=false
		* Ex: /howmany?complete=all

### O que cada item de um inventário deverá possuir ###

* ID (numérico e auto incremento)
* Nome (Alfanumérico)
* Categoria (Alfanumérico)
* Valor do bem inventariado (numérico com 2 casas decimais)
* Indicativo boleano de que o cadastro está completo

Cada bem inventariado deverá ter uma estrutura semelhante a essa: 

```json
{
	"id": 123,
	"name": "Automóvel Gol 2015",
	"category": "Automóveis",
	"value": 12345.67,
	"complete": true
}
```

### Observações ###

Os dados poderão ser gravados em um banco de dados (recomendado) ou salvos em memória.

## Requisitos ##

* Java 8
* Spring Boot

# Avaliação #

* Resposta da API
* Qualidade do código
* Organização e documentação

# Como enviar #

* Para envio do código: Basta fazer um fork desse projeto, e ao final do teste abrir um Pull Request ou publicar o seu código em um repositório com visibilidade pública, como Github ou Bitbucket

